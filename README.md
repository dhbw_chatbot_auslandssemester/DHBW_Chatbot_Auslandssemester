<h1> Das Chatbot Projekt </h1>
<b>Dies ist nur eine Kurzinformation. Weitere Informationen entnehmen Sie bitte der Projektdokumentation. Diese ist ebenfalls in diesem Respo abrufbar.</b>
<h2>Autoren</h2>
Christian Scheub,
Irina Lupanov,
Melvin Müller

<h2> Starten der Plattform:</h2> 
Um die Plattform aufzurufen gehen Sie auf die Seite:  https://chatbot.dhbw-stuttgart.de/admin/login/default <br>
E-Mail: christian.scheub@gmail.com
Passwort: #ÄdminChatbot00

<h2> Starten des Chatbots</h2>
Chatbot Vollbild Ansicht: https://chatbot.dhbw-stuttgart.de/lite/dhbw_auslandssemester_final/?m=channel-web&v=Fullscreen&options=%7B%22hideWidget%22%3Atrue%2C%22config%22%3A%7B%22enableReset%22%3Atrue%2C%22enableTranscriptDownload%22%3Atrue%7D%7D
<br>
Chatbot Implementation auf einer leeren Webseite: https://dhbw-studenten-projekte.de/bot_sites/DHBW_Server_BotPress.html
<br>
Chatbot Implementation auf einer Webseite mit Inhalt: https://dhbw-studenten-projekte.de/bot_sites/bot_site.html



<h2> Kurzbeschreibung </h2>
Der Startimpuls des vorliegenden Projektes bestand in der Entwicklung eines Vorgehensmodells im Rahmen des Integrationsseminars an der DHBW Stuttgart. Im zugehörigen Modul „Projekt“ wurde auf Basis des konzipierten Vorgehensmodells aus dem Integrationsseminar der Auftrag gegeben, einen Chatbot für die DHBW Stuttgart zu entwickeln. Der Themenschwerpunkt des zu entwickelnden Chatbots bezieht sich dabei auf die Beantwortung von Fragen zu dem Thema „Auslandssemester“. Die Umsetzung erfolgte mithilfe der Botpress Plattform, welche auf einem lokalen Server von der DHBW gehostet wird. <br>
Das TGZ-Archiv sowie den Ordner "Botpress Bot" ist hier lediglich zu Dokumentationszwecken. 
Das TGZ-Archiv können Sie verwenden, um den Chatbot in die Botpress Plattform zu importieren. Dieser ist jedoch bereits eingerichtet auf der Plattform. Dementsprechend ist dies erst notwendig, falls Sie diesen löschen. 
<h2> Angewendete Technologien </h2>
<ul>
<li>Red Hat OS </li>
<li> HTML </li>
<li> CSS </li>
<li> JavaScript</li>
<li> SSH </li>
<li> OpenSSL </li>
<li> TLS </li>
<li> NGNIX </li>
<li> Botpress Plattform</li>
<li> GIT</li>
</ul>
<h2>Lizenzen </h2>
Teile des Codes sind aus den folgenden Bibilothekten oder referenzieren auf diese. <br>
<b> Bootstrap (MIT Lizenz)</b> https://getbootstrap.com/docs/4.0/about/license/ <br>
<b> Botpress (GNU Affero General Public License v3.0)</b> https://github.com/botpress/botpress/blob/master/LICENSE <br>
<b> FontAwesome (MIT Lizenz) </b> https://fontawesome.com/v4/license/ <br>
<b> jQuery (MIT Lizenz)</b> https://jquery.org/license/
